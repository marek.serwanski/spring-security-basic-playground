package pl.mserwanski.springsecurityplayground;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@ComponentScan("pl.mserwanski.springsecurityplayground")
public class WebConfig implements WebMvcConfigurer {



}
