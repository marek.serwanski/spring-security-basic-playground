package pl.mserwanski.springsecurityplayground.error;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.mserwanski.springsecurityplayground.user.UsernameAlreadyExistException;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.PRECONDITION_FAILED;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@RestControllerAdvice
public class EndpointsErrorHandler {

    @ResponseStatus(UNAUTHORIZED)
    @ExceptionHandler(UsernameNotFoundException.class)
    public ErrorResponse handleUserNotFound(Exception e) {
        return new ErrorResponse(e);
    }

    @ResponseStatus(UNAUTHORIZED)
    @ExceptionHandler(BadCredentialsException.class)
    public ErrorResponse handleWrongPassword(Exception e) {
        return new ErrorResponse(e);
    }

    @ResponseStatus(PRECONDITION_FAILED)
    @ExceptionHandler(UsernameAlreadyExistException.class)
    public ErrorResponse handleUsernameExist(Exception e) {
        return new ErrorResponse(e);
    }

    @ResponseStatus(FORBIDDEN)
    @ExceptionHandler(AccessDeniedException.class)
    public ErrorResponse handleUnauthorizedException(Exception e) {
        return new ErrorResponse(e);
    }

    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorResponse handleGlobalError(Exception e) {
        return new ErrorResponse(e, "Info for admin: unknown server error - no defined handler cought exception :(");
    }

}
