package pl.mserwanski.springsecurityplayground.error;

//Just box for response message return, with additional info providing possibility
public class ErrorResponse {

    private final String msg;
    private String additionalInfo;

    ErrorResponse(Exception e) {
        this.msg = e.getMessage();
    }

    ErrorResponse(Exception e, String additionalInfo) {
        this.msg = e.getMessage();
        this.additionalInfo = additionalInfo;
    }

    public String getMsg() {
        return msg;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}
