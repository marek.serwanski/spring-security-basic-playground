package pl.mserwanski.springsecurityplayground.healthcheck;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static pl.mserwanski.springsecurityplayground.healthcheck.HealthStatus.HEALTH_OK;


@RestController
public class HealthCheckEndpoint {

    @RequestMapping("/status")
    public HealthStatus getStatus() {
        return HEALTH_OK();
    }
}
