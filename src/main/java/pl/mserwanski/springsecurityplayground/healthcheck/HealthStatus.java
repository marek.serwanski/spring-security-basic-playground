package pl.mserwanski.springsecurityplayground.healthcheck;

public class HealthStatus {

    private final boolean running;

    private HealthStatus(boolean running) {
        this.running = running;
    }

    static HealthStatus HEALTH_OK() {
        return new HealthStatus(true);
    }

    public boolean isRunning() {
        return running;
    }
}
