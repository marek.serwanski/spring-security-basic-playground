package pl.mserwanski.springsecurityplayground.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

import static java.util.Arrays.asList;

@Configuration
public class CorsConfig {

    private final static List<String> ALLOWED_ORIGINS = asList("http://localhost:4200");
    private final static List<String> ALLOWED_METHODS = asList("GET", "POST", "PUT", "DELETE");
    private final static List<String> ALLOWED_HEADERS = asList(
            "Access-Control-Allow-Origin", "Access-Control-Allow-Methods", "Access-Control-Allow-Headers",
            "Access-Control-Expose-Headers", "Access-Control-Allow-Credentials",
            "x-requested-with", "Content-Type", "x-auth-token", "x-csrf-token"
    );

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", getConfiguration());
        return source;
    }

    private CorsConfiguration getConfiguration() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(ALLOWED_ORIGINS);
        configuration.setAllowedMethods(ALLOWED_METHODS);
        configuration.setAllowedHeaders(ALLOWED_HEADERS);
        return configuration;
    }
}
