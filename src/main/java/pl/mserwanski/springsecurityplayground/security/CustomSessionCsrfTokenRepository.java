package pl.mserwanski.springsecurityplayground.security;

import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.DefaultCsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.UUID;

// Class similar to standard spring HttpSessionCsrfTokenRepository.
// One logic extension: saveToken method also sets token in header and allow external apps read it (expose-header)
// E.g. Andular needs it
public class CustomSessionCsrfTokenRepository implements CsrfTokenRepository {

    private static final String CSRF_PARAMETER_NAME = "_csrf";
    private static final String CSRF_HEADER_NAME = "x-csrf-token";
    private static final String CSRF_ACCESS_HEADER_NAME = "Access-Control-Expose-Headers";
    private static final String CSRF_TOKEN_ATTR_NAME = HttpSessionCsrfTokenRepository.class
            .getName().concat(".CSRF_TOKEN");

    @Override
    public CsrfToken generateToken(HttpServletRequest request) {
        String token = UUID.randomUUID().toString();
        return new DefaultCsrfToken(CSRF_HEADER_NAME, CSRF_PARAMETER_NAME, token);
    }

    @Override
    public void saveToken(CsrfToken token, HttpServletRequest request, HttpServletResponse response) {
        if (token == null) {
            HttpSession session = request.getSession(false);
            if (session != null) {
                session.removeAttribute(CSRF_TOKEN_ATTR_NAME);
            }
        }
        else {
            HttpSession session = request.getSession();
            session.setAttribute(CSRF_TOKEN_ATTR_NAME, token);
            response.setHeader(CSRF_HEADER_NAME, token.getToken());
            response.setHeader(CSRF_ACCESS_HEADER_NAME, CSRF_HEADER_NAME);
        }
    }

    @Override
    public CsrfToken loadToken(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return null;
        }
        return (CsrfToken) session.getAttribute(CSRF_TOKEN_ATTR_NAME);
    }
}


