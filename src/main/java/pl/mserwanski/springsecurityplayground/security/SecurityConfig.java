package pl.mserwanski.springsecurityplayground.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.session.jdbc.config.annotation.web.http.EnableJdbcHttpSession;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import pl.mserwanski.springsecurityplayground.user.CustomUserDetailsService;

@Configuration
@EnableWebSecurity(debug = true)
@EnableJdbcHttpSession
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    //CSRF_FREE_URI = {"/login", "/register"}

    private static final String[] UNSECURRED_URI = {"/status", "/login", "/register", "/logoff", "/console/**"};
    private static final String[] CSRF_FREE_URI = {"/status", "/login", "/register", "/logoff", "/console/**", "/xsrf_playground/unsecure/edit"};
    private static final String XSRF_TOKEN = "AUTH-TOKEN";

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        //authorization
            http.authorizeRequests()
                .antMatchers(UNSECURRED_URI).permitAll()
                .anyRequest().authenticated()
                .and()
                .httpBasic();

        //CSRF config
        http.csrf()
            .ignoringAntMatchers(CSRF_FREE_URI)
            .csrfTokenRepository(new CustomSessionCsrfTokenRepository());

        //enable CORS
        http.cors();

        //other
        http.logout().deleteCookies("JSESSIONID", XSRF_TOKEN)
            .and()
            .headers().frameOptions().disable();    //for h2 console
    }

    @Bean
    public CsrfTokenRepository csrfTokenRepository() {
        HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
        repository.setHeaderName(XSRF_TOKEN);
        return repository;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(customUserDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public HeaderHttpSessionStrategy sessionStrategy() {
        return new HeaderHttpSessionStrategy();
    }

    //to makes everything easier, I made injection "old-fashioned" way
    @Autowired
    UserDetailsService customUserDetailsService;
}
