package pl.mserwanski.springsecurityplayground.task;

import org.hibernate.annotations.NaturalId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

import static java.util.UUID.randomUUID;

@Entity
class Task implements Serializable {

    private static final long serialVersionUID = 40L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // NaturalId is handy for identyfing tasks by frontend. Not good idea to reveal database id.
    @NaturalId
    @Column(nullable = false, unique = true)
    private final String hashId;

    // set flag to false, instead of permanent delete - common business practise
    private boolean active;

    private String title;
    private String username;
    private String description;

    Task() {
        this.active = true;
        this.hashId = randomUUID().toString();
    }

    Task(String hashId) {
        this.active = true;
        this.hashId = hashId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getHashId() {
        return hashId;
    }
}
