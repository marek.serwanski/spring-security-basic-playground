package pl.mserwanski.springsecurityplayground.task;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class TaskConfig {

    @Bean
    public TaskMapper taskMapper() {
        return new TaskMapper();
    }

    @Bean
    public TaskService taskService(TaskRepository taskRepository, TaskMapper taskMapper) {
        return new TaskService(taskRepository, taskMapper);
    }
}
