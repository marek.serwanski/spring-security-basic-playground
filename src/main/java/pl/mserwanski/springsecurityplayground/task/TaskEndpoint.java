package pl.mserwanski.springsecurityplayground.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
public class TaskEndpoint {

    private final TaskService taskService;

    @Autowired
    public TaskEndpoint(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/task")
    public List<TaskDTO> getTasks(Principal principal) {
        return taskService.getTasks(principal.getName());
    }

    @PostMapping("/task")
    public void saveTask(Principal principal, @RequestBody TaskDTO taskDTO) {
        taskService.saveTask(principal.getName(), taskDTO);
    }

    @DeleteMapping("/task/{hashId}")
    public void deleteTask(Principal principal, @PathVariable String hashId) {
        taskService.deleteTask(principal.getName(), hashId);
    }
}
