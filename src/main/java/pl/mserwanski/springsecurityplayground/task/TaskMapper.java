package pl.mserwanski.springsecurityplayground.task;

class TaskMapper {

    TaskDTO map(Task task) {
        TaskDTO result = new TaskDTO();
        result.setDescription(task.getDescription());
        result.setTitle(task.getTitle());
        result.setHashId(task.getHashId());

        return result;
    }

    Task mapToNewTask(TaskDTO taskDTO, String username) {
        Task result = new Task();
        result.setDescription(taskDTO.getDescription());
        result.setTitle(taskDTO.getTitle());
        result.setUsername(username);

        return result;
    }
}
