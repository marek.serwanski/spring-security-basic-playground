package pl.mserwanski.springsecurityplayground.task;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

interface TaskRepository extends JpaRepository<Task, Long> {

    List<Task> findByUsernameAndActive(String username, boolean active);

    Optional<Task> findByHashId(String hashId);

}
