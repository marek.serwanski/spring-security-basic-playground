package pl.mserwanski.springsecurityplayground.task;

import org.springframework.security.access.AccessDeniedException;

import java.util.List;
import java.util.stream.Collectors;

class TaskService {

    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;

    TaskService(TaskRepository taskRepository, TaskMapper taskMapper) {
        this.taskRepository = taskRepository;
        this.taskMapper = taskMapper;
    }

    List<TaskDTO> getTasks(String username) {
        return taskRepository.findByUsernameAndActive(username, true).stream().map(taskMapper::map)
                .collect(Collectors.toList());
    }

    void saveTask(String username, TaskDTO taskDTO) {
        taskRepository.save(taskMapper.mapToNewTask(taskDTO, username));
    }

    void deleteTask(String loggedUser, String hashId) {
        taskRepository.findByHashId(hashId).ifPresent(task -> {
            //Check if currently logged user is owner of task. Otherwise random logged user can delete any task
            if (!task.getUsername().equals(loggedUser)) {
                throw new AccessDeniedException("You are not owner of task " + hashId);
            }
            task.setActive(false);
            taskRepository.save(task);
        });
    }
}
