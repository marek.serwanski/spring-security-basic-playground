package pl.mserwanski.springsecurityplayground.user;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class UserConfig {

    @Bean
    public CustomUserDetailsService customUserDetailsService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        return new CustomUserDetailsService(userRepository, passwordEncoder);
    }

    @Bean
    public UserManualAuthenticator manualAuthenticator(DaoAuthenticationProvider authenticationProvider) {
        return new UserManualAuthenticator(authenticationProvider);
    }

}
