package pl.mserwanski.springsecurityplayground.user;

//Transport purposes between controllers layer and services
public class UserDTO {

    //cannot be final, empty constructor needed
    private String username;
    private String password;

    public UserDTO() {
    }

    public UserDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

}
