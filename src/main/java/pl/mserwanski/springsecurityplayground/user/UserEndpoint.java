package pl.mserwanski.springsecurityplayground.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

@RestController
public class UserEndpoint {

    private final CustomUserDetailsService customUserDetailsService;
    private final UserManualAuthenticator userManualAuthenticator;

    @Autowired
    public UserEndpoint(CustomUserDetailsService customUserDetailsService, UserManualAuthenticator userManualAuthenticator) {
        this.customUserDetailsService = customUserDetailsService;
        this.userManualAuthenticator = userManualAuthenticator;
    }

    @PostMapping("/login")
    public void login(@RequestBody UserDTO userDTO, HttpServletRequest request, HttpServletResponse response) {
        SecurityContext securityContext = userManualAuthenticator.authenticateUser(userDTO);
        request.getSession(true).setAttribute(SPRING_SECURITY_CONTEXT_KEY, securityContext);

        //allow frontend to save tokens
        response.setHeader("Access-Control-Expose-Headers", "x-auth-token, x-csrf-token");
    }

    @PostMapping("/register")
    public void register(@RequestBody UserDTO userDTO) throws UsernameAlreadyExistException {
        customUserDetailsService.createUser(userDTO);
    }

    @PostMapping("/logoff")
    public void logout() {
        SecurityContextHolder.clearContext();
    }
}
