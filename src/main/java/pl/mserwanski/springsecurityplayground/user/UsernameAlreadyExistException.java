package pl.mserwanski.springsecurityplayground.user;

public class UsernameAlreadyExistException extends Exception {

    public UsernameAlreadyExistException(String username) {
        super("This username already exist: " + username);
    }
}
