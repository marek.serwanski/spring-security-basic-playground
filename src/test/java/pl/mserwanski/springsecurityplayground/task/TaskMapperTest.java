package pl.mserwanski.springsecurityplayground.task;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.mserwanski.springsecurityplayground.user.TestUserFactory.defaultUsername;

public class TaskMapperTest {

    TaskMapper mapper = new TaskMapper();

    @Test
    public void shouldMapToTaskDto() {
        TaskDTO result = mapper.map(TestTaskFactory.defaultTestTask());

        assertThat(result.getHashId()).isEqualTo(TestTaskFactory.HASH_ID);
        assertThat(result.getTitle()).isEqualTo(TestTaskFactory.TEST_TITLE);
        assertThat(result.getDescription()).isEqualTo(TestTaskFactory.TEST_DESC);
    }

    @Test
    public void shouldMapToNewTask() {
        Task result = mapper.mapToNewTask(TestTaskFactory.defaultTestTaskDTO(), defaultUsername());

        assertThat(result.getHashId()).isNotEmpty();
        assertThat(result.getTitle()).isEqualTo(TestTaskFactory.TEST_TITLE);
        assertThat(result.getDescription()).isEqualTo(TestTaskFactory.TEST_DESC);
    }
}
