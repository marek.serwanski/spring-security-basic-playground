package pl.mserwanski.springsecurityplayground.task;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.access.AccessDeniedException;

import java.util.Arrays;
import java.util.List;

import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.mserwanski.springsecurityplayground.user.TestUserFactory.defaultUsername;

@RunWith(MockitoJUnitRunner.class)
public class TaskServiceTest {

    @Mock
    TaskRepository taskRepository;
    @Mock
    TaskMapper taskMapper = new TaskMapper();

    @InjectMocks
    TaskService service;

    @Test
    public void shouldSaveTask() {
        ArgumentCaptor<Task> captor = ArgumentCaptor.forClass(Task.class);
        when(taskMapper.mapToNewTask(any(), anyString())).thenReturn(TestTaskFactory.defaultTestTask());

        service.saveTask(defaultUsername(), TestTaskFactory.defaultTestTaskDTO());

        verify(taskRepository).save(captor.capture());
        Task savedTask = captor.getValue();
        assertThat(savedTask.getUsername()).isEqualTo(defaultUsername());
        assertThat(savedTask.getTitle()).isEqualTo(TestTaskFactory.TEST_TITLE);
        assertThat(savedTask.getDescription()).isEqualTo(TestTaskFactory.TEST_DESC);
    }

    @Test
    public void shouldGetTasks() {
        String username = defaultUsername();
        when(taskRepository.findByUsernameAndActive(username, true)).thenReturn(Arrays.asList(TestTaskFactory.defaultTestTask()));
        when(taskMapper.map(any())).thenReturn(TestTaskFactory.defaultTestTaskDTO());

        List<TaskDTO> result = service.getTasks(username);

        assertThat(result.size()).isEqualTo(1);
        TaskDTO resultDTO = result.get(0);
        assertThat(resultDTO.getTitle()).isEqualTo(TestTaskFactory.TEST_TITLE);
        assertThat(resultDTO.getDescription()).isEqualTo(TestTaskFactory.TEST_DESC);
    }

    @Test
    public void shouldDeleteTask() {
        ArgumentCaptor<Task> captor = ArgumentCaptor.forClass(Task.class);
        when(taskRepository.findByHashId(TestTaskFactory.HASH_ID)).thenReturn(of(TestTaskFactory.defaultTestTask()));

        service.deleteTask(defaultUsername(), TestTaskFactory.HASH_ID);

        verify(taskRepository).save(captor.capture());
        Task deletedTask = captor.getValue();
        assertThat(deletedTask.getHashId()).isEqualTo(TestTaskFactory.HASH_ID);
        assertThat(deletedTask.isActive()).isFalse();
    }

    @Test(expected = AccessDeniedException.class)
    public void shouldNotDeleteOtherUserTask() {
        when(taskRepository.findByHashId(TestTaskFactory.HASH_ID)).thenReturn(of(TestTaskFactory.defaultTestTask()));
        service.deleteTask("hacker", TestTaskFactory.HASH_ID);
    }
}