package pl.mserwanski.springsecurityplayground.task;

import static pl.mserwanski.springsecurityplayground.user.TestUserFactory.defaultUsername;

class TestTaskFactory {

    static final String TEST_TITLE = "title";
    static final String TEST_DESC = "description";
    static final String HASH_ID = "qwer1234";

    static TaskDTO defaultTestTaskDTO() {
        TaskDTO result = new TaskDTO();
        result.setTitle(TEST_TITLE);
        result.setDescription(TEST_DESC);
        result.setHashId(HASH_ID);
        return result;
    }

    static Task defaultTestTask() {
        Task task = new Task(HASH_ID);
        task.setUsername(defaultUsername());
        task.setTitle(TEST_TITLE);
        task.setDescription(TEST_DESC);

        return task;
    }
}
