package pl.mserwanski.springsecurityplayground.user;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CustomUserDetailsServiceTest {

    @Mock
    UserRepository userRepository;
    @Mock
    PasswordEncoder passwordEncoder;

    @InjectMocks
    CustomUserDetailsService service;

    @Test
    public void shouldLoadByUserName() {
        when(userRepository.findByUsername(TestUserFactory.TEST_USERNAME)).thenReturn(TestUserFactory.defaultUser());
        UserDetails userDetails = service.loadUserByUsername(TestUserFactory.TEST_USERNAME);

        assertThat(userDetails.getUsername()).isEqualTo(TestUserFactory.TEST_USERNAME);
        assertThat(userDetails.getPassword()).isEqualTo(TestUserFactory.TEST_PASSWORD);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void shouldNotLoadUser() {
        service.loadUserByUsername(TestUserFactory.TEST_USERNAME);
    }

    @Test
    public void shuldSaveUser() throws Exception {
        //given
        ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);
        when(passwordEncoder.encode(anyString())).thenReturn(TestUserFactory.TEST_ENCODED_PASSWORD);

        //when
        service.createUser(TestUserFactory.defaultUserDTO());

        //then
        verify(userRepository).save(captor.capture());
        User user = captor.getValue();

        assertThat(user.getUsername()).isEqualTo(TestUserFactory.TEST_USERNAME);
        assertThat(user.getPassword()).isEqualTo(TestUserFactory.TEST_ENCODED_PASSWORD);
    }
}