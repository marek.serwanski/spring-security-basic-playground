package pl.mserwanski.springsecurityplayground.user;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import java.util.Optional;

public class TestUserFactory {

    static final String TEST_USERNAME = "test";
    static final String TEST_PASSWORD = "test";
    static final String TEST_ENCODED_PASSWORD = "encoded_test";

    static UserDTO defaultUserDTO() {
        return new UserDTO(TEST_USERNAME, TEST_PASSWORD);
    }

    static Optional<User> defaultUser() {
        User user = new User();
        user.setUsername(TEST_USERNAME);
        user.setPassword(TEST_PASSWORD);
        return Optional.of(user);
    }

    //for other services
    public static String defaultUsername() {
        return TEST_USERNAME;
    }

    static Authentication defaultAuthentication() {
        return new UsernamePasswordAuthenticationToken("test", "test");
    }
}
