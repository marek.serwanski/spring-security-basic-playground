package pl.mserwanski.springsecurityplayground.user;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static pl.mserwanski.springsecurityplayground.user.TestUserFactory.TEST_USERNAME;
import static pl.mserwanski.springsecurityplayground.user.TestUserFactory.defaultAuthentication;
import static pl.mserwanski.springsecurityplayground.user.TestUserFactory.defaultUserDTO;

@RunWith(MockitoJUnitRunner.class)
public class UserManualAuthenticatorTest {

    @Mock
    DaoAuthenticationProvider authenticationProvider;
    @InjectMocks
    UserManualAuthenticator authenticator;

    @Test
    public void shouldAuthenticateUser() {

        when(authenticationProvider.authenticate(any(Authentication.class))).thenReturn(defaultAuthentication());

        SecurityContext securityContext = authenticator.authenticateUser(defaultUserDTO());

        Authentication authentication = securityContext.getAuthentication();
        assertThat(authentication.getPrincipal()).isEqualTo(TEST_USERNAME);
        assertThat(authentication.getCredentials()).isEqualTo(TEST_USERNAME);
    }

}